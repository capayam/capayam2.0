SIR MOHD ALI BIN MOHD ISA
BOOK RESERVATION
M3CS2514B

GROUP MEMBER
1.	MUHAMMAD AIZAT BIN IBRAHIM
2.	MOHAMAD FAHMI BIN MOHAMAD SHUHARI
3.	KHALEEDA BIN IRO ISWONO
4.	SURIATI BINTI KASSIM

For our group project, we have decided to create a website for book reservation. 
In this website we will create 2 different interface which is for user and administrator.
This website also has login and logout for user so that the administrator may know the user detailed if they want to borrow the books. 

User
•	List all books
•	Search books by genre, author and language
•	Book reservation
•	Cancel reservation
•	Books detailed
•	User detailed

Administrator
•	Add book
•	Delete book
•	Edit books detailed
•	Show reserved books
•	Statistic
•	Admin detailed

